#Lecture en mode CSV
from collections import OrderedDict
import os

import csv

#Fonction permettant de de modifier 
def trancode(ligne) :
    sortie = {}
    typed ,varianted,versiond = ligne['type_variante_version'].split(",")
    sortie["address_titulaire"] = ligne["address"]
    sortie["nom"] = ligne["name"]
    sortie["prenom"] = ligne["firstname"]
    sortie["immatriculation"] = ligne["immat"]
    sortie["date_immatriculation"] = ligne["date_immat"]
    sortie["vin"] = ligne["vin"]
    sortie["marque"] = ligne["marque"]
    sortie["denomination_commerciale"] = ligne["denomination"]
    sortie["couleur"] = ligne["couleur"]
    sortie["carrosserie"] = ligne["carrosserie"]
    sortie["categorie"] = ligne["categorie"]
    sortie["cylindre"] = ligne["cylindree"]
    sortie["energie"] = ligne["energy"]
    sortie["places"] = ligne["places"]
    sortie["poids"] = ligne["poids"]
    sortie["puissance"] = ligne["puissance"]
    sortie["type"] = typed
    sortie["variante"]  = varianted
    sortie["version"] = versiond
    return sortie

#Fonction permettant de saisie le fichier à traiter
def saisie() :
    print("Saisir le nom du fichier CSV :")
    saisie = input()
    print("Saisir le delimiteur :")
    delimiteur = input()

    while os.path.exists(saisie) == False | os.path.isfile(saisie) != True :

        print("Fichier introuvable ou pas au bon format")
        print("Saisir le nom du fichier CSV :")
        saisie = input()
    return saisie,delimiteur


saisie,delimiteur = saisie()

with open(saisie) as cvsfile: #Ouverture en mode lecture du fichier à traiter

    reader = csv.DictReader(cvsfile,delimiter='|') #Lecture du fichier
    with open('sortieauto.csv','w',newline='') as csvfile2: #ouverture du fichier de destination en mode écriture
        fildnames = ["address_titulaire","nom","prenom","immatriculation","date_immatriculation","vin","marque","denomination_commerciale","couleur","carrosserie","categorie","cylindre","energie","places","poids","puissance","type","variante","version"]
        writer = csv.DictWriter(csvfile2,delimiter=delimiteur, fieldnames=fildnames)
        writer.writeheader()
        for row in reader:
            value2 = trancode(row)
            writer.writerow(value2)



    
